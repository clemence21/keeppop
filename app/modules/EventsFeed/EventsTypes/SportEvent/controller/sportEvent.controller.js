const db = require("../../../config/sequelize.config");
const SportEvent = db.sportEvents;
const SportType = db.sportTypes;
const Op = db.Sequelize.Op;


// Create and Save a new sportEvent
exports.create = (req, res) => {
  // Validate request
  if (!req.body.title || !req.body.description || !req.body.date )  {
    res.status(400).send({
      message: "Missing either title, description or date "
    });
    return;
  }

  // Create an SportEvent
  const sportEvent = {
    title: req.body.title,
    description: req.body.description,
    is_published: req.body.is_published ? req.body.is_published : 0,
    is_public: req.body.is_public ? req.body.is_public : 0,
    is_outside: req.body.is_outside ? req.body.is_outside : 0,
    date: req.body.date,
    place: req.body.place,
    number_of_players: req.body.number_of_players
  };

  // Save Sport event in the database
  SportEvent.create(sportEvent)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Event."
      });
    });
};

// Export and Save a new sportType
exports.createSportType = (sportEventId, sportType) => {
    return SportType.create({
        name: {
            type: Sequelize.STRING
        },
        number_of_player: {
            type: Sequelize.INTEGER
        },
        is_published: {
            type: Sequelize.BOOLEAN
        },
        is_outside: {
            type: Sequelize.BOOLEAN
        },
        description: {
            type: Sequelize.STRING
        },
        rule: {
            type: Sequelize.STRING
        }
    })
      .then((comment) => {
        console.log("Comment was created" + JSON.stringify(comment, null, 4));
        return comment;
      })
      .catch((err) => {
        console.log("An error occured while creating comment: ", err);
      });
};

// Get sportEvents for a given sportType
exports.findSportEventsBySportTypeId = (sportTypeId) => {
    return SportEvent.findByPk(sportTypeId, { include: ["comments"] })
      .then((sportEvent) => {
        return sportEvent;
      })
      .catch((err) => {
        console.log("An error occured while finding sport Events: ", err);
      });
};

// Retrieve all SportEvent from the database where condition.
exports.findAllArticles = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Article.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving articles."
      });
    });
};


// Find a single sportEvent with an id 
exports.findOne = (req, res) => {
  const id = req.params.id;

  SportEvent.findByPk(id)
    .then(data => {
      res.send(data);
      console.log('coucou'+ "mon sport event "+ data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving sport event with id=" + id +'   '+ err
      });
    });
};

// Update a Sport Event by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  SportEvent.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Sport Event was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Sport Event with id=${id}. Maybe Sport Event was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Sport Event with id=" + id
      });
    });
};


// Delete a sport event with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  SportEvent.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Sport event was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete sport event with id=${id}. Maybe sport event was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Sport event with id=" + id
      });
    });
};


// Delete all SportEvents from the database.
exports.deleteAll = (req, res) => {
  SportEvent.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Sport events were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all Sport events."
      });
    });
};

// Find all published and public Sport Event
exports.findAllPublishedAndPublicSportEvent = (req, res) => {
  SportEvent.findAll({ where: { is_published: true } && {is_public: true} })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving published and public sport Event."
      });
    });
};

// Find all published Sport Event 
exports.findAllPublishedSportEvent = (req, res) => {
  SportEvent.findAll({ where: { is_published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving published sport event."
      });
    });
};

// Find all public Sport Event 
exports.findAllPublicSportEvent = (req, res) => {
  SportEvent.findAll({ where: { is_public: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving public sport events."
      });
    });
};