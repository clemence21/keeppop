module.exports = (sequelize,  Sequelize) => {
    const CelebrationType = sequelize.define("celebration_type", {
        name: {
            type: Sequelize.STRING
        },
        description: {
            type: Sequelize.STRING
        },
        max_participants: {
            type: Sequelize.INTEGER
        }
    })

    return CelebrationType;
};