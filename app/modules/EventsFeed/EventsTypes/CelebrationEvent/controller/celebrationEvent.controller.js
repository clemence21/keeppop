const db = require("../../../../../config/sequelize.config");
const CelebrationEvent = db.celebrationEvents;
const Op = db.Sequelize.Op;

// Create and Save a celebration event
exports.createCelebrationEvent = (req, res) => {
    // Create a celebration Event
    const celebrationEvent = {
      max_invited: req.body.max_invited,
      max_participants_confirmed: req.body.max_participants_confirmed,
    };
  
    // Save celebration event in the database
    CelebrationEvent.create(celebrationEvent)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while creating the celebration."
        });
      });
  };

// Find a single celebration Event with an id 
exports.findOneCelebrationEventById = (req, res) => {
  const id = req.params.id;

  CelebrationEvent.findByPk(id)
    .then(data => {
      res.send(data);
      console.log('coucou'+ "my celebration event "+ data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving celebration event with id=" + id +'   '+ err
      });
    });
};

// Update an event by the id in the request
exports.updateCelebrationEventById = (req, res) => {
  const id = req.params.id;

  CelebrationEvent.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Celebration event was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update celebration Event with id=${id}. Maybe this celebration Event was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating celebration event with id=" + id
      });
    });
};

// Delete a celebration event with the specified id in the request
exports.deleteCelebrationEventById = (req, res) => {
  const id = req.params.id;

  CelebrationEvent.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Celebration Event was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete celebration Event with id=${id}. Maybe celebration Event was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete celebration event with id=" + id
      });
    });
};

// Delete all celebration events from the database.
exports.deleteAllCelebrationEvents = (req, res) => {
  CelebrationEvent.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} celebration Events were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all celebration events."
      });
    });
};


//// Find all celebration event whre max participant confirmed is 50
exports.findAllCelebrationEventsWithMaxParticipants50 = (req, res) => {
  CelebrationEvent.findAll({ where: { max_participants_confirmed: 50 } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving celebration event with 50 max participants"
      });
    });
};