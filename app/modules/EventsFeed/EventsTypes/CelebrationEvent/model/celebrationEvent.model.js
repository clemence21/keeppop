module.exports = (sequelize,  Sequelize) => {
    const CelebrationEvent = sequelize.define("celebration_event", {
        max_invited: {
            type: Sequelize.INTEGER
        },
        max_participants_confirmed: {
            type: Sequelize.INTEGER
        }
    })
    return CelebrationEvent;
};