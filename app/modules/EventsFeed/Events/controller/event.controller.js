const db = require("../../../../config/sequelize.config");
const Event = db.events;
const SportEvent = db.sportEvents;
const CelebrationEvent = db.celebrationEvents;
const ConcertEvent = db.concertEvents;
const Op = db.Sequelize.Op;


// Create and Save an event
exports.createEvent = (req, res) => {
  // Validate request
  if (!req.body.title) {
    res.status(400).send({
      message: "Title can not be empty!"
    });
    return;
  }

  // Create an Event
  const event = {
    title: req.body.title,
    description: req.body.description,
    date: req.body.date,
    place: req.body.place,
    is_published: req.body.is_published ? req.body.is_published : 0,
    is_outside: req.body.is_comment_enabled ? req.body.is_comment_enabled : 0,
    is_public: req.body.is_public ? req.body.is_public : 0,
  };

  // Save Event in the database
  Event.create(event)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Event."
      });
    });
};


// Retrieve all Events from the database where condition.
exports.findAllEvents = (req, res) => {
  const title = req.query.title;
  var condition = title ? { title: { [Op.like]: `%${title}%` } } : null;

  Event.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving events."
      });
    });
};


// Find a single event with an id 
exports.findOneEventById = (req, res) => {
  const id = req.params.id;

  Event.findByPk(id)
    .then(data => {
      res.send(data);
      console.log('coucou'+ "my event "+ data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving event with id=" + id +'   '+ err
      });
    });
};

// Update an event by the id in the request
exports.updateEventById = (req, res) => {
  const id = req.params.id;

  Event.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Event was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Event with id=${id}. Maybe Event was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating event with id=" + id
      });
    });
};


// Delete an event with the specified id in the request
exports.deleteEventById = (req, res) => {
  const id = req.params.id;

  Event.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Event was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Event with id=${id}. Maybe Event was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Article with id=" + id
      });
    });
};


// Delete all events from the database.
exports.deleteAllEvents = (req, res) => {
  Event.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Events were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all events."
      });
    });
};

// Find all published and public Events 
exports.findAllPublishedAndPublicEvents = (req, res) => {
  Event.findAll({ where: { is_published: true } && {is_public: true} })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving published and public events."
      });
    });
};

// Find all published Events
exports.findAllPublishedEvents = (req, res) => {
  Event.findAll({ where: { is_published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving published events."
      });
    });
};

// Find all public Events
exports.findAllPublicEvents = (req, res) => {
  Event.findAll({ where: { is_public: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving public events."
      });
    });
};