const events = require("../controller/event.controller");
var router = require("express").Router();

module.exports = function(app) {

    // POST ROUTES 

    // Create a new event
    router.post("/create-event", events.createEvent);

    // GET ROUTES 
  
    // Retrieve all events
    router.get("/", events.findAllEvents);
  
    // Retrieve all published articles
    router.get("/published", events.findAllPublishedEvents);
    
    // Retrieve all published and public articles 
    router.get("/public", events.findAllPublishedAndPublicEvents);

    // Retrieve a single articles with id
    router.get("/:id", events.findOneEventById);

    // PUT ROUTES
    // Update an event with id
    router.put("/:id", events.updateEventById);
  
    // DELETE ROUTES
    // Delete an event with id
    router.delete("/:id", events.deleteEventById);
  
    // Delete all Events
    router.delete("/", events.deleteAllEvents);
    
    app.use('/api/events', router);
  };