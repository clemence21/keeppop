module.exports = (sequelize, Sequelize) => {
    const Profile = sequelize.define("profiles", {
      name: {
        type: Sequelize.STRING
      },
      avatar: {
        type: Sequelize.STRING
      },
      licence: {
          type: Sequelize.STRING
      },
    });
  
    return Profile;
  };