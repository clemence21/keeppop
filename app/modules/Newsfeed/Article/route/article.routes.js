const articles = require("../controller/article.controller");
var router = require("express").Router();

module.exports = function(app) {

    // POST ROUTES 

    // Create a new articles
    router.post("/create-article", articles.create);

    // Create a new comment 
    router.post("/:id/create-comment", articles.createComment);

    // GET ROUTES 
  
    // Retrieve all articles
    router.get("/", articles.findAllArticles);
  
    // Retrieve all published articles
    router.get("/published", articles.findAllPublished);
    
    // Retrieve all published and public articles 
    router.get("/public", articles.findAllPublishedAndPublic);

    // Retrieve a single articles with id
    router.get("/:id", articles.findOne);

    // Retrieve comments from a single articles with id
    router.get("/:id/comments", articles.findCommentsByArticleId);

    // PUT ROUTES
    // Update a article with id
    router.put("/:id", articles.update);
  
    // DELETE ROUTES
    // Delete an article with id
    router.delete("/:id", articles.delete);
  
    // Delete all Articles
    router.delete("/", articles.deleteAll);
    
    app.use('/api/articles', router);
  };