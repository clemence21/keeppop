const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");

const app = express();

var corsOptions = {
  origin: "http://localhost:8081"
};

app.use(cors(corsOptions));

// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

const db = require("./config/sequelize.config");
const Role = db.role;
const SportType = db.sportTypes;
const CelebrationType = db.celebrationTypes;
const Article = db.articles;

db.sequelize.sync({force: true}).then(() => {
  console.log('Drop and Resync Db');
  initial();
});

app.get("/", (req, res) => {
  res.json({ message: "Welcome to mefezjgzlerjgzerlm." });
});


// ROUTES 
require('./modules/Auth/routes/auth.routes')(app);
require('./modules/User/routes/user.routes')(app);
require('./modules/Newsfeed/Article/route/article.routes')(app);
require('./modules/EventsFeed/Events/routes/event.routes')(app);

// set port, listen for requests
const PORT = process.env.PORT || 8080;

app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

function initial() {
  Role.create({
    id: 1,
    name: "user"
  });
 
  Role.create({
    id: 2,
    name: "moderator"
  });
 
  Role.create({
    id: 3,
    name: "admin"
  });

  SportType.create({
    id: 1,
    name: "Football",
    description: "Le football /futbol/, ou soccer, est un sport collectif qui se joue avec un ballon sphérique entre deux équipes de onze joueurs. Elles s'opposent sur un terrain rectangulaire délimité, équipé de buts définis sur les largeurs opposées.",
    rule: "Les règles sont au nombre de dix-sept, et concernent chacune un aspect spécifique du jeu. On fait parfois référence, de manière badine, à une 18e loi concernant le « sens commun ou esprit du jeu »1. Il s'agit en fait de souligner qu'un arbitre doit interpréter les 17 lois de manière à conserver l'esprit du jeu, celui-ci étant plus important que la lettre. De fait, les lois laissent place à une certaine interprétation2. Les coutumes font qu'un certain nombre d'actions ou sanctions sont communément considérées comme admises alors qu'elles ne sont pas pour autant décrites dans les lois du jeu."
  });

  SportType.create({
    id: 2,
    name: "Volleyball",
    description:"Le volley-ball, ou volleyball, est un sport collectif mettant en jeu deux équipes de 6 joueurs séparés par un filet, qui s'affrontent avec un ballon sur un terrain rectangulaire de 18 mètres de long sur 9 mètres de large.",
    rule:"Règles du jeu. Les points sont marqués soit en faisant tomber le ballon sur le terrain de l'équipe adverse, soit quand l'adversaire commet une faute. La première équipe à atteindre 25 points (avec 2 points d'écart minimum) gagne le set et la première équipe qui gagne trois sets gagne le match."
  }); 
  
  SportType.create({
    id: 3,
    name: "Waterpolo",
    description:"Le water-polo est un sport à la dépense énergétique importante qui demande d’avoir une excellente forme physique, de maîtriser le crawl water-polo et d’être particulièrement endurant",
    rule:"Le temps de jeu Quatre périodes de 8 minutes de temps réel avec 2 minutes de repos entre chaque période et 5 minutes entre la 2ème et 3ème période. Le temps de jeu est arrêté à chaque coup de sifflet de l’arbitre. Chaque équipe a le droit à deux arrêts de jeu d’une durée d’une minute pour chaque rencontre.Le temps limite de possession du ballon2 tableaux à affichage numérique indiquent le temps de possession du ballon. L'équipe a 30 secondes pour lancer le ballon à partir du moment où elle le récupère.L’arbitrageDeux arbitres sont situés des deux côtés du champ de jeu assisté de 2 ou 3 officiels de table (chronométreur et secrétaire). Lors d'une faute, les arbitres indiquent le camp de l'équipe qui a commis la faute."
  });
  SportType.create({
    id: 4,
    name:"Rugby",
    description:"Sport qui oppose deux équipes de quinze joueurs qui se disputent de la main ou du pied un ballon ovale qu'il faut déposer derrière la ligne de but de l'adversaire ou faire passer entre les deux poteaux de but au-dessus d'une barre transversale.",
    rule:"Les règles du rugby à XV en font un sport très particulier. Il s'agit d'un sport d'équipe, de combat, avec ballon. Sa spécificité principale est de mêler jeu à la main et combat physique parfois violent."
  });

  CelebrationType.create({
    id: 1,
    name: 'Pendaison de cremaillère',
    description: "La pendaison de crémaillère était donc une façon de dire aux amis et à la famille : « La maison est finie ; nous pouvons festoyer ensemble. » Aujourd'hui, cette expression signifie inviter les amis à un repas, ou à une fête, pour célébrer un emménagement (même lorsque l'habitation n'a pas de cheminée).",
    max_participants: 25,
  });
  CelebrationType.create({
    id: 2,
    name: 'After work',
    description: "C'est ce concept «cool» et anglo-saxon qui consiste à aller partager une bière, ou un verre de vin, entre collègues après le travail. ... Un moment convivial qui peut avoir lieu soit entre les murs de l'entreprise - dans un espace autorisé!",
    max_participants: 35,
  });
  CelebrationType.create({
    id: 3,
    name: 'Anniversaire',
    description: "Un anniversaire est la date dans l'année à laquelle un événement est survenu, habituellement une naissance. Il est fréquent, dans de nombreuses cultures, de célébrer l'anniversaire de la naissance de ses proches en organisant une fête et en offrant des cadeaux à la personne concernée.",
    max_participants: 50,
  });

  Article.create({
    id: 1,
    title:"Notre top 100 des albums 2020",
    description: "Si l’année a été, par la force des choses, pauvre en concerts, elle n’en a pas moins été riche en nouvelles prouesses discographiques. Voici le choix de la rédaction des Inrockuptibles.Dossier coordonné par Carole Boinet, Rémi Boiteux, Vincent Brunner Naomi Clément, Maxime Delcourt, Arnaud Ducome, Adrien Durand, Valentin Gény, Noémie Lecoq, Brice Miclet, François Moreau, Jérôme Provençal, Xavier Ridel, Sophie Rosemont, Patrick Thévenin, Franck Vergeade. Pris au piège de cette grande centrifugeuse qu’est l’existence, Daniel Blumberg a trouvé son salut dans les arts plastiques et, surtout, l’expérimentation musicale pour se sortir d’une dépression qui en aurait laissé plus d’un·e sur le carreau. Pour son deuxième album, le Londonien s’entoure d'un quartet d'amis fidèles (déjà au générique de Minus) pour emmener la pop du côté des territoires infinis de l’improvisation.",
    category: "musique",
    is_public:1,
    is_published:1,
    is_comment_enabled:1
  });
  Article.create({
    id: 2,
    title:"“Euphoria” : l'épisode spécial débarque le 6 décembre",
    description: "Alors que le tournage de la saison 2 d'Euphoria n'a même pas encore commencé, HBO diffusera un épisode spécial le 6 décembre. On peut se demander s'il s'agit d'un épisode spécial Noël ou spécial Covid... En deux volets, Trouble Don’t Last Always permettra aux fans de prendre leur mal en patience et de supporter les retards considérables pris par la production en raison de l'épidémie de coronavirus. En effet, Sam Levinson - le créateur - a même eu le temps de tourner un film avec son actrice Zendaya durant le premier confinement. Le tournage de la deuxième saison d'Euphoria devrait commencer début 2021.",
    category: "series",
    is_public:1,
    is_published:1,
    is_comment_enabled:1
  });

}
